import React from 'react';
import ReactDOM from 'react-dom'; 
import App from './App';
import * as serviceWorker from './serviceWorker';
import Amplify from 'aws-amplify';
import { Provider }  from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import  configureStore, { history }   from './store'; 
// import { PersistGate } from "redux-persist/integration/react"; 

Amplify.configure({
  Auth: {
    mandatorySignId : true,
    region: process.env.REACT_APP_COGNITO_REGION,
    userPoolId: process.env.REACT_APP_COGNITO_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_COGNITO_APP_CLIENT_ID,    
  }
})

const store = configureStore();

ReactDOM.render( 
    <Provider store={store} > 
      <ConnectedRouter history={history}> 
        {/* <PersistGate loading={null} persistor={persistStor} > */}
          <App />
        {/* </PersistGate> */}
      </ConnectedRouter>
    </Provider> ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
